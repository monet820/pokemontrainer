import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterTrainerComponent } from './components/forms/register-trainer/register-trainer.component';
import { PokemonCatalogueComponent } from './components/pages/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDetailComponent } from './components/pages/pokemon-detail/pokemon-detail.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: "login",
    component: RegisterTrainerComponent
  },
  {
    path: "trainer",
    component: TrainerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pokemon-catalogue",
    component: PokemonCatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pokemon-detail/:id",
    component: PokemonDetailComponent,
    canActivate: [AuthGuard]
  },

  // redirect to login as default.
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
