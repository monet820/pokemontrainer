import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { PokemonCatalogueComponent } from './components/pages/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDetailComponent } from './components/pages/pokemon-detail/pokemon-detail.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { NavigationComponent } from './components/shared/navigation/navigation.component'
import { ReactiveFormsModule } from '@angular/forms';
// API service
import { HttpClientModule } from '@angular/common/http';
import { RegisterTrainerComponent } from './components/forms/register-trainer/register-trainer.component';

@NgModule({
  declarations: [
    AppComponent,
    TrainerComponent,
    PokemonCatalogueComponent,
    PokemonDetailComponent,
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    RegisterTrainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { 
public trainerName = "";
  
}
