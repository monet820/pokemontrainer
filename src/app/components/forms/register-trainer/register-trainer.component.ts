import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { TrainerService } from 'src/app/services/trainer/trainer.service';

@Component({
  selector: 'app-register-trainer',
  templateUrl: './register-trainer.component.html',
  styleUrls: ['./register-trainer.component.scss']
})

export class RegisterTrainerComponent implements OnInit{

  signupForm: FormGroup;

  constructor(private session: SessionService, private trainer: TrainerService, private router: Router) { }

  ngOnInit() {
    if(this.session.getTrainer() != null)
    {
      this.router.navigateByUrl("/pokemon-catalogue")
    } else {

      this.signupForm = new FormGroup({
        "trainerName": new FormControl(null, [Validators.required, Validators.maxLength(20), Validators.minLength(3)])
      });
    };
  }

  async onSubmit() {
    location.reload();
    this.session.saveTrainer(this.signupForm.value.trainerName);
    this.trainer.saveTrainerLocal();
    this.trainer.clearTrainerCollection();
    this.router.navigateByUrl("/pokemon-catalogue");
  }
}
