import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PokemonCard } from 'src/app/models/pokemonCard';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.scss']
})
export class PokemonCatalogueComponent {
  
  public pokemons: PokemonCard = null;

  public currentLimit = 1050;
  public nextOffset = 0;

  constructor(private service: PokemonService) { }

async ngOnInit(){
    this.getPokemons();
  }

  async getPokemons(){
    this.pokemons = await this.service.fetchPokemons(this.nextOffset, this.currentLimit);
    this.nextOffset += this.currentLimit;
    console.log(this.pokemons);
  }
}
