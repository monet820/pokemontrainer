import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/Pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';
import {TrainerService} from "../../../services/trainer/trainer.service";

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent {
  pokemonID: number;

  constructor(private serviceRoute: ActivatedRoute, private service: PokemonService, public trainer: TrainerService) {
    this.pokemonID = this.serviceRoute.snapshot.params.id
  }

  public pokemon: Pokemon = null;

  async ngOnInit() {
    this.pokemon = await this.service.fetchPokemon(this.pokemonID);
  }

  onClickMe() {
   this.trainer.addPokemonToTrainer(this.pokemon);
   alert(this.pokemon.name + " was added to collection");
  }
}


