import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/Pokemon';
import { TrainerService } from 'src/app/services/trainer/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})

export class TrainerComponent {
 
  pokemonList: Pokemon[] = [];

  constructor(public trainer: TrainerService) { 
   }

   async ngOnInit() {
     this.pokemonList = this.trainer.getTrainerCollection();
    console.log(this.trainer.getTrainerCollection());
  }
}
