import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

// Variables for the header
@Input() title: string = "Pokemon Trainer";


  constructor() { }

  ngOnInit(): void {
  }

}
