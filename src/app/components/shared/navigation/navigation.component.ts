import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { TrainerService } from 'src/app/services/trainer/trainer.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public currentTrainer: string = "";

  constructor(private session: SessionService) { }

  async ngOnInit() {
    this.getTrainer();
  }

  async clearUser(){
    location.reload();
    this.session.clearTrainer();
  }

  async getTrainer(){
    this.currentTrainer = this.session.getTrainer();
    return this.currentTrainer;
  }

}
