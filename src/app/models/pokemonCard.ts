import {Pokemon} from "../models/Pokemon"

export interface PokemonCard{
    count: number;
    next: string;
    previous: string;
    results: Pokemon;
    image?: string;
}