import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/Pokemon';
import { PokemonCard } from '../models/pokemonCard';

@Injectable({
  providedIn: 'root'
})

export class PokemonService {

  constructor(private http: HttpClient) { }

  fetchPokemon(pokemonID: number): Promise<Pokemon> {
    return this.http.get<Pokemon>(environment.pokemonURL+"/"+pokemonID).toPromise()
  }

  fetchPokemons(offset:number, limit:number) : Promise<PokemonCard> {
    return this.http.get<PokemonCard>(environment.pokemonsURL+"?offset="+offset+"&limit="+limit).toPromise()
  }
}
