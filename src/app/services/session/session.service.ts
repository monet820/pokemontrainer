import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class SessionService {

  private currentTrainer: string = "";

  constructor() { }
  
  getTrainer() {
    this.currentTrainer = localStorage.getItem("currentTrainer");
    return this.currentTrainer;
  }

  saveTrainer(name: any ) {
    localStorage.setItem("currentTrainer", JSON.stringify(name))
  }

  clearTrainer(){
    localStorage.clear();
  }

 
}
