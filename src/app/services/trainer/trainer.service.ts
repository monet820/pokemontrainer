import { Injectable } from '@angular/core';
import { Pokemon } from 'src/app/models/Pokemon';

@Injectable({
  providedIn: 'root'
})

export class TrainerService {
     
    private trainerCollection: Pokemon[] = []

  saveTrainerLocal(){
    localStorage.setItem("collection",JSON.stringify(this.trainerCollection));
  }

 getTrainerCollection(){
    return this.trainerCollection=JSON.parse(localStorage.getItem("collection"));;
  }

  addPokemonToTrainer(poke: Pokemon){
    this.trainerCollection.push(poke);
    this.saveTrainerLocal();
  }

  clearTrainerCollection(){
    this.trainerCollection = [];    
  }
}